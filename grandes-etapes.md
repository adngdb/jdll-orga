# Grandes étapes

Les grandes étapes dans l'organisation en amont des JDLL.

## À 6 mois de l'événement

Première réunion pour constitution de l'équipe d'organisation.

*Objectifs*

 - Constitution de l'équipe
 - Mise à jour des adresses email pour la mailing list
 - Répartition des rôles de chacun dans les pôles

 ## À 5 mois

 Deuxième réunion pour définition de l'évènement.

 *Objectifs*

  - Choix du thèmes
  - Préparation de l'appel à contributions
  - Définition des deadlines (contributions, communication... )

## À 4 mois

Troisième réunion pour affinage de la communication.

*Objectifs*

 - Validation et diffusion de l'appel à contributions
 - Liste des personnalités à inviter
 - Validation d'une chartre graphique (site, flyers, promo, gobelets)

## À 3 mois

Quatrième réunion pour évaluation de l'avancement.

*Objectifs*

 - Point sur les propositions reçues
 - Stratégie de relance
 - Préparation pour le salon Primevère
 - Préparation de la buvette et de la cantine

## À 2 mois

Cinquième réunion pour finalisation du planning.

*Objectifs*

 - Finalisation et validation du planning complet de l'évènement
 - Préparation de la communication blog / réseaux sociaux
 - Préparation de la logistique pour l'évènement
 - Recrutement de bénévoles

## À 1 mois

Sixième réunion pour finalisation des détails de l'organisation.

*Objectifs*

 - Validation de la logistique (réseau, signalétique, matériel divers)
 - Planning des bénévoles
 - Plannification de l'organisation pendant l'évènement

## Au jour J

Faire l'évènement !

## 1 mois après

Septième réunion pour bilan.

*Objectifs*

 - Bilan sur ce qui s'est bien ou mal passé
 - Propositions d'améliorations pour l'édition suivante
